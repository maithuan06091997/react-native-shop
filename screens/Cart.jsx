import React, { useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import ListCart from "../components/Cart/ListCart";
import TotalCart from "../components/Cart/TotalCart";

function Cart(props) {
  const [listCart, setListCart] = useState([
    {
      id: 1,
      name: "Son môi Hallyu1",
      img:
        "https://www.kenhnews.com/wp-content/uploads/2020/08/1596604559_971_Top-3-dong-son-moi-Han-Quoc-hot-nhat-2020.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 2,
      name: "Son môi Hallyu2",
      img:
        "https://photo-1-baomoi.zadn.vn/w1000_r1/2018_12_07_7_28890048/d0bc447086366f683627.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
      quantity: 1,
    },
  ]);
  return (
    <ScrollView>
      <View style={styles.body}>
        <ListCart listCart={listCart} />
      </View>
      <View style={styles.footer}>
        <TotalCart />
      </View>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  footer: {
    height: 50,
  },
});
export default Cart;
