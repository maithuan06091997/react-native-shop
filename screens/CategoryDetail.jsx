import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import ProductItem from "../components/ProductItem";

function CategoryDetail(props) {
  const { navigation } = props;
  const [listProduct, setListProduct] = useState([
    {
      id: 1,
      name: "Son môi Hallyu1",
      img:
        "https://www.kenhnews.com/wp-content/uploads/2020/08/1596604559_971_Top-3-dong-son-moi-Han-Quoc-hot-nhat-2020.jpg",
      price: "500.000",
    },
    {
      id: 2,
      name: "Son môi Hallyu2",
      img:
        "https://photo-1-baomoi.zadn.vn/w1000_r1/2018_12_07_7_28890048/d0bc447086366f683627.jpg",
      price: "500.000",
    },
    {
      id: 3,
      name: "Son môi Hallyu3",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
      price: "500.000",
    },
    {
      id: 4,
      name: "Son môi Hallyu4",
      img: "https://static.ilike.com.vn/articles/thumb/s0/0/0/1/33184.jpg?v=2",
      price: "500.000",
    },
  ]);
  return (
    <FlatList
      data={listProduct}
      renderItem={({ item }) => (
        <View style={styles.flexCloums}>
          <ProductItem product={item} />
        </View>
      )}
      keyExtractor={(item) => item.id + ""}
      numColumns="2"
      style={styles.container}
    />
  );
}
const styles = StyleSheet.create({
  flexCloums: {
    flex: 1,
  },
  container: {
    padding: 8,
  },
});
export default CategoryDetail;
