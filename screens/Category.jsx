import React, { useState } from "react";
import { FlatList, StyleSheet } from "react-native";
import CategoryListItem from "../components/CategoryListItem";

function Category(props) {
  const { navigation } = props;
  const [listCategory, setCategory] = useState([
    {
      id: 1,
      name: "Son môi Hallyu",
      img:
        "https://www.kenhnews.com/wp-content/uploads/2020/08/1596604559_971_Top-3-dong-son-moi-Han-Quoc-hot-nhat-2020.jpg",
    },
    {
      id: 2,
      name: "Son môi 3CE",
      img:
        "https://photo-1-baomoi.zadn.vn/w1000_r1/2018_12_07_7_28890048/d0bc447086366f683627.jpg",
    },
    {
      id: 3,
      name: "Son môi Recipe",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
    },
    {
      id: 4,
      name: "Son môi Cinema",
      img: "https://static.ilike.com.vn/articles/thumb/s0/0/0/1/33184.jpg?v=2",
    },
    {
      id: 5,
      name: "Son môi Blending",
      img:
        "https://netdep.com.vn/upload/tin-tic/review-son/mousse-blending.jpg",
    },
    {
      id: 6,
      name: "Son môi Velvet",
      img:
        "https://api-np.fime.vn/data/images/upload/20190114/B7C424491B45492BBCB4078BD2BCBE25",
    },
    {
      id: 7,
      name: "Son môi Hallyu",
      img:
        "https://www.kenhnews.com/wp-content/uploads/2020/08/1596604559_971_Top-3-dong-son-moi-Han-Quoc-hot-nhat-2020.jpg",
    },
    {
      id: 8,
      name: "Son môi 3CE",
      img:
        "https://photo-1-baomoi.zadn.vn/w1000_r1/2018_12_07_7_28890048/d0bc447086366f683627.jpg",
    },
    {
      id: 9,
      name: "Son môi Recipe",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
    },
    {
      id: 10,
      name: "Son môi Cinema",
      img: "https://static.ilike.com.vn/articles/thumb/s0/0/0/1/33184.jpg?v=2",
    },
    {
      id: 11,
      name: "Son môi Blending",
      img:
        "https://netdep.com.vn/upload/tin-tic/review-son/mousse-blending.jpg",
    },
    {
      id: 12,
      name: "Son môi Velvet",
      img:
        "https://api-np.fime.vn/data/images/upload/20190114/B7C424491B45492BBCB4078BD2BCBE25",
    },
    {
      id: 13,
      name: "Son môi Hallyu",
      img:
        "https://www.kenhnews.com/wp-content/uploads/2020/08/1596604559_971_Top-3-dong-son-moi-Han-Quoc-hot-nhat-2020.jpg",
    },
    {
      id: 14,
      name: "Son môi 3CE",
      img:
        "https://photo-1-baomoi.zadn.vn/w1000_r1/2018_12_07_7_28890048/d0bc447086366f683627.jpg",
    },
    {
      id: 15,
      name: "Son môi Recipe",
      img:
        "https://tinacosmetic.com/wp-content/uploads/2018/04/son-3ce-mood-1.jpg",
    },
    {
      id: 16,
      name: "Son môi Cinema",
      img: "https://static.ilike.com.vn/articles/thumb/s0/0/0/1/33184.jpg?v=2",
    },
    {
      id: 17,
      name: "Son môi Blending",
      img:
        "https://netdep.com.vn/upload/tin-tic/review-son/mousse-blending.jpg",
    },
    {
      id: 18,
      name: "Son môi Velvet",
      img:
        "https://api-np.fime.vn/data/images/upload/20190114/B7C424491B45492BBCB4078BD2BCBE25",
    },
  ]);
  return (
    <FlatList
      data={listCategory}
      renderItem={({ item }) => (
        <CategoryListItem
          category={item}
          navLink={() =>
            navigation.navigate("Details", {
              categoryName: item.name,
            })
          }
        />
      )}
      keyExtractor={(item) => String(item.id)}
      numColumns="2"
      style={styles.container}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 8,
  },
});

export default Category;
