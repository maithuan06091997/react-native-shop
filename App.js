import * as React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  CartScreen,
  HomeScreen,
  OrderScreen,
  SettingScreen,
} from "./appNavigator";

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: "#fdb3d4",
        }}
      >
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarLabel: "My home",

            tabBarIcon: ({ focused }) => {
              return (
                <Ionicons
                  name="ios-home"
                  size={20}
                  color={focused ? "#fdb3d4" : "#8e8e8f"}
                />
              );
            },
          }}
        />
        <Tab.Screen
          name="Cart"
          component={CartScreen}
          options={{
            tabBarLabel: "Cart",
            tabBarIcon: ({ focused }) => {
              return (
                <Ionicons
                  name="ios-cart"
                  size={20}
                  color={focused ? "#fdb3d4" : "#8e8e8f"}
                />
              );
            },
          }}
        />
        <Tab.Screen
          name="Order"
          component={OrderScreen}
          options={{
            tabBarLabel: "Order",
            tabBarIcon: ({ focused }) => {
              return (
                <Ionicons
                  name="ios-reorder"
                  size={20}
                  color={focused ? "#fdb3d4" : "#8e8e8f"}
                />
              );
            },
          }}
        />
        <Tab.Screen
          name="Setting"
          component={SettingScreen}
          options={{
            tabBarLabel: "Setting",
            tabBarIcon: ({ focused }) => {
              return (
                <Ionicons
                  name="ios-settings"
                  size={20}
                  color={focused ? "#fdb3d4" : "#8e8e8f"}
                />
              );
            },
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
