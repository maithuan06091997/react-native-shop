import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";

function CartItem(props) {
  const { cart } = props;
  return (
    <View style={styles.container}>
      <View style={styles.padding}>
        <View style={styles.flexDirectionRow}>
          <Image source={cart.img} style={styles.cartImage} />
          <View>
            <Text style={styles.name}>{cart.name}</Text>
            <Text style={styles.price}>{cart.price}</Text>
          </View>
        </View>
        <View style={styles.flexDirectionRow}>
          <TouchableOpacity style={styles.btnQuantity} activeOpacity={0.5}>
            <Text style={styles.textQuantity}>-</Text>
          </TouchableOpacity>
          <Text style={styles.quantity}>{cart.quantity}</Text>
          <TouchableOpacity style={styles.btnQuantity} activeOpacity={0.5}>
            <Text style={styles.textQuantity}>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  padding: {
    shadowColor: "#000000",
    shadowOpacity: "0.1",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 10,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  flexDirectionRow: {
    flexDirection: "row",
    alignItems: "center",
  },
  name: {
    color: "#555555",
    fontSize: 14,
    fontWeight: "500",
    marginBottom: 5,
  },
  cartImage: {
    width: 100,
    height: 60,
    marginRight: 15,
  },
  price: {
    fontSize: 14,
    color: "#fdb3d4",
  },
  btnQuantity: {
    borderRadius: "100%",
    width: "22px",
    height: "22px",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#fdb3d4",
    textAlign: "center",
  },
  textQuantity: {
    color: "#555",
    fontWeight: "600",
  },
  quantity: {
    marginHorizontal: 8,
    fontWeight: "600",
    color: "#fdb3d4",
  },
});

export default CartItem;
