import React from "react";
import { FlatList } from "react-native";
import CartItem from "./CartItem";

function ListCart(props) {
  const { listCart } = props;
  return (
    <FlatList
      data={listCart}
      renderItem={({ item }) => <CartItem cart={item} />}
      keyExtractor={(item) => item.id + ""}
    />
  );
}

export default ListCart;
