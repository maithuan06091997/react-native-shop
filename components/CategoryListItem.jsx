import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
function CategoryListItem(props) {
  const { navLink, category } = props;
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={navLink} style={{ flex: 1 }}>
      <View style={styles.container}>
        <Image source={category.img} style={styles.categoryImage} />
        <Text style={styles.text}>{category.name}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    shadowColor: "#000000",
    shadowOpacity: "0.1",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 10,
    borderRadius: 4,
    marginHorizontal: 4,
    flex: 1,
    alignItems: "center",
    marginBottom: 12,
    backgroundColor: "#fff",
  },
  categoryImage: {
    height: 120,
    width: "100%",
    borderTopEndRadius: 4,
    borderTopStartRadius: 4,
  },
  text: {
    fontWeight: "500",
    marginTop: 10,
    marginBottom: 8,
    color: "#fdb3d4",
  },
});

export default CategoryListItem;
