import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from "react-native";

function ProductItem(props) {
  const { product } = props;
  return (
    <View style={styles.container}>
      <View style={styles.showDow}>
        <Image source={product.img} style={styles.imageProduct} />
        <Text style={styles.name}>{product.name}</Text>
        <View style={styles.cart}>
          <Text style={styles.price}>{product.price}</Text>
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => {
              alert("Mua thành công");
            }}
          >
            <Text style={styles.cartText}>Mua +</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
  showDow: {
    shadowColor: "#000000",
    shadowOpacity: "0.1",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 10,
    borderRadius: 4,
  },
  imageProduct: {
    flex: 1,
    height: 120,
    resizeMode: "cover",
    borderTopEndRadius: 4,
    borderTopStartRadius: 4,
  },
  name: {
    fontSize: 14,
    marginTop: 12,
    marginBottom: 4,
    color: "#555",
    fontWeight: "500",
    paddingHorizontal: 8,
  },
  cart: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 8,
    paddingBottom: 8,
  },
  cartText: {
    fontSize: 16,
    color: "#fdb3d4",
  },
  price: {
    fontSize: 14,
    color: "#fdb3d4",
  },
});

export default ProductItem;
