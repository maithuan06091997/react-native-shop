import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Category from "./screens/Category";
import CategoryDetail from "./screens/CategoryDetail";
import Cart from "./screens/Cart";
import Order from "./screens/Order";
import Setting from "./screens/Setting";

const HomeStack = createStackNavigator();
const CartStack = createStackNavigator();
const OrderStack = createStackNavigator();
const SettingStack = createStackNavigator();
function styleHeader(title) {
  return {
    headerTitle: title,
    headerStyle: {
      backgroundColor: "#fdb3d4",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  };
}
export function HomeScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={Category}
        options={styleHeader("Shop")}
      />
      <HomeStack.Screen
        name="Details"
        component={CategoryDetail}
        options={({ route }) => styleHeader(route.params.categoryName)}
      />
    </HomeStack.Navigator>
  );
}

export function CartScreen() {
  return (
    <CartStack.Navigator>
      <CartStack.Screen
        name="Cart"
        component={Cart}
        options={styleHeader("My Cart")}
      />
    </CartStack.Navigator>
  );
}

export function OrderScreen() {
  return (
    <OrderStack.Navigator>
      <OrderStack.Screen
        name="Order"
        component={Order}
        options={{
          headerTitle: "My Order",
        }}
      />
    </OrderStack.Navigator>
  );
}

export function SettingScreen() {
  return (
    <SettingStack.Navigator>
      <SettingStack.Screen
        name="Setting"
        component={Setting}
        options={{
          headerTitle: "My Setting",
        }}
      />
    </SettingStack.Navigator>
  );
}
